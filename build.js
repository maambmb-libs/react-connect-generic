const path  = require( "path" );
const fs    = require( "fs" );
const util  = require( "util" );
const babel = require( "babel-core" );

async function build() {
    var { code } = await util.promisify( babel.transformFile )( path.join( __dirname, "index.js" ), {
        presets : [ "es2015", "react" ],
        plugins : [ "transform-object-rest-spread" ],
    } );
    await util.promisify( fs.writeFile )( path.join( __dirname, "dist.js" ), code, "utf-8" );
}

build();
