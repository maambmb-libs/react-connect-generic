const React = require( "react" );

const DEFAULT_CTX_PROP = "___react_connect_generic_ctx_prop";

const dataContext = React.createContext();

const connect = ( mapStateToProps = () => ({}), mergeProps = () => ({}), { ctxProp = DEFAULT_CTX_PROP } ) => (component) => {

    class Component extends React.Component {

        constructor() {
            super();

            this.state  = {};
            this.__subs = {};
            this.__id   = 0;
            this.__ctx  = null;

            this.__oldSource = {};
            this.__newSource = {};
        }

        _subscribe( fn ) {
            var id = this.id ++;
            this.__subs[ id ] = fn;
            return () => delete this.__subs[ id ];
        }

        shouldComponentUpdate( nextProps ) {

            this.__newSource = {
                ... nextProps,
                ... mapStateToProps( this.__ctx, nextProps )
            };

            for (let i in this.__newSource) 
                if (!(i in this.oldSource)) 
                    return true;
            for (let i in this.__oldSource) 
                if (this.__newSource[i] !== this.__oldSource[i]) 
                    return true;

            return false;
        }

        componentDidMount() {
            this.__ctx = this.props[ ctxProp ];
            this.__unsubscribe = this.props[ ctxProp ].subscribe( () => {
                this.setState({});
                for( var sub of this.__subs )
                    sub();
            } );
        }

        componentWillUnmount() {
            this.__unsubscribe();
        }

        render() {

            this.__oldSource = this.__newSource;

            var props = {
                ... this.__oldSource,
                ... mergeProps( this.__ctx, this.__oldSource, this.props )
            };

            return (
                <dataContext.Provider value={ this.__subscribe.bind( this ) }>
                    { React.createElement( component, props ) }
                </dataContext.Provider>
            );
        }

    }

    return (props) => (
        <dataContext.Consumer>
            { (ctx) => React.createElement( Component, { ... props, [ ctxProp ] : ctx } ) }
        </dataContext.Consumer>
    );

};

const Provider = ( { children, ctx } ) => (
    <dataContext.Provider value={ ctx }>
        { children }
    </dataContext.Provider>
);

module.exports = { connect, Provider };
