const React       = require( "react" );
const { Manager } = require( "mini-subscribe" );

const subscribeContext = React.createContext();

const connect = ( { subscribeProp = "___RGCSK___", getStateProps = () => ({}), actionProps = {}, component } ) => {

    class ConnectedComponent extends React.Component {

        constructor() {

            super();

            this.state       = {};
            this.manager     = new Manager();
            this.subscribeFn = (fn) => this.manager.subscribe( fn );

        }

        shouldComponentUpdate( nextProps ) {

            var newData = Object.assign( {}, nextProps, getStateProps( nextProps ) );

            for (let i in newData) 
                if (!(i in this.oldProps)) 
                    return true;

            for (let i in this.oldProps) 
                if (newData[i] !== this.oldProps[i]) 
                    return true;

            return false;

        }

        componentDidMount() {

            this.unsubscribe = this.props[ subscribeProp ]( () => { 
                this.setState( {} );
                this.manager.dispatch();
            } );

        }

        componentWillUnmount() {

            this.unsubscribe();

        }

        render() {

            this.oldProps = Object.assign( {},
                this.props,
                getStateProps( this.props ),
                actionProps,
            );

            return (
                <subscribeContext.Provider value={ this.subscribeFn }>
                    { React.createElement( component, this.oldProps ) }
                </subscribeContext.Provider>
            );

        }

    }

    return (props) => ( 
        <subscribeContext.Consumer>
            { (fn) => React.createElement( ConnectedComponent, { ... props, [ subscribeProp ] : fn } ) }
        </subscribeContext.Consumer>
    );

};

const Provider = (props) => (
    <subscribeContext.Provider value={ props.subscribe }>
        { props.children }
    </subscribeContext.Provider>
);

module.exports = { 
    connect,
    Provider
};
